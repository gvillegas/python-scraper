#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import re
import time
import sys
from urllib2 import urlopen
import requests
from bs4 import BeautifulSoup
from mysql import connector
import re
import unicodedata



'''
GLOBALES
'''
global session
#############
def elimina_tildes(s):
    return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))


'''
CLASES
'''


class Producto:
    def __init__(self):
        self._partnumber = None
        self._us = None
        self._valor = None
        self._stock = None
        self._descripcion = None
        self._codigotg = None

    @property
    def partnumber(self):
        return self._partnumber

    @partnumber.setter
    def partnumber(self, partnumber):
        self._partnumber = partnumber

    @property
    def us(self):
        return self._us

    @us.setter
    def us(self, us):
        self._us = us

    @property
    def valor(self):
        return self._valor

    @valor.setter
    def valor(self, valor):
        self._valor = valor

    @property
    def stock(self):
        return self._valor

    @stock.setter
    def stock(self, stock):
        self._stock = stock

    @property
    def descripcion(self):
        return self._descripcion

    @descripcion.setter
    def descripcion(self, descripcion):
        descripcion = elimina_tildes(descripcion)
        self._descripcion = descripcion

    @property
    def codigotg(self):
        return self._codigotg

    @codigotg.setter
    def codigotg(self, codigotg):
        self._codigotg = codigotg

    def __call__(self):
        self.producto()




'''
ARREGLOS DE WEB
'''
tgMarcas = []
VpMarcas = {}

'''''
DATA DE CONEXIONES
'''''
usuario = 'xxx'
clave = 'xxxx'
try:
    con = connector.Connect(user='xx',
                            password='xxx!',
                            database='xxxx',
                            connect_timeout=3600,
                            host='xxx'
                            )

    cur = con.cursor()

except Exception, e:
    print '[-] Error en conexion db: ', e.message
    sys.exit(0)

''''
DATOS TECNOGLOBAL
'''''
URL = 'http://www.tecnoglobal.cl/default.asp'
session = requests.Session()

login_data = {
    'command': "lo_login",
    'command2': "",
    'lo_usuario': usuario,
    'lo_passwd': clave,
}
r = session.post(URL, data=login_data)
r = session.get('http://www.tecnoglobal.cl/default.asp')
#############
global tg_us #DOLAR TG
#############
getUs = session.get(URL)
tg_us = BeautifulSoup(getUs.text, "html.parser")
tg_us = BeautifulSoup(str(tg_us), "html.parser")
tg_us = tg_us.find("div", {"id": "divDatos"})
tg_us = BeautifulSoup(str(tg_us), "html.parser").find_all('td')
for y in tg_us:
   if 'Dolar TG:' in y.text:
        match = re.search(r"\d{3}", y.text)
        tg_us = int(match.group())
        break

if tg_us == 0 or tg_us == None:
    print('Valor TG_US es 0 o NULO SYSTEM.EXIT(0)')
    sys.exit(0)

#########################
def BuscarProductos(values):
    post_data = {
        'command': "pa_marcas",
        'command2': "ma_categorias",
        'ho_dato': "",
        'ca_dato': "",
        'se_dato': "",
        'no_dato': "",
        'pr_dato': "",
        'ay_dato': "",
        'bu_texto': "",
        'bu_marca': values[2],  # marca
        'bu_familia': values[0],  # categoria
        'bu_subfam': values[1],  # subcategoria
        'bu_stock': "",
        'bu_pagina': "",
        'bu_tipobus': "M",
        'bu_oferta': "",
        'cc_listado': "",
        'cc_opcion': "",
        'cc_nrocot': "",
        'cc_codart': "",
        'cc_cantidad': "",
        'cc_codsucursal': "",
        'cc_sucursal': "",
        'cc_oc': "",
        'cc_notaventa': "",
        'cc_observacion': "",
        'cc_coladespacho': "",
        'cc_indformadespacho': "",
        'cc_despachosparciales': "",
        'cc_usaflete': "",
        'cc_clientefinal': "",
        'cc_rutclientefinal': "",
        'cc_razonsocialclientefinal': "",
        'cc_direccionclientefinal': "",
        'cc_ciudadclientefinal': "",
        'cc_nombrecontactoclientefinal': "",
        'cc_telefonocontactoclientefinal': "",
        'cc_correocontactoclientefinal': "",
        'mo_tipo': "",
        'mo_nvp_NotaVenta': "",
        'mo_num_Factura': "",
        'mo_num_NotaCredito': "",
        'mo_erma_Rma': "",
        'of_dato': "",
        'textobusca': "",
        'marcabusca': "",  # marca
        'ca_familia': "",
        'ca_subfam': ""
    }
    b = session.post(URL, data=post_data)
    html = BeautifulSoup(b.text, "html.parser")

    if 'class="tpoindex"' in str(html):
        pags = BeautifulSoup(str(html), "html.parser")
        pags = pags.find_all('div', class_='tpoindex')

        pags = str(pags)
        pags = BeautifulSoup(pags, "html.parser")
        pags = pags.find_all('font', attrs={'color': '#cc0000'})
        pags = len(pags)
        productos = []
        for x in range(1, pags + 1):
            post_data = {
                'command': "pa_marcas",
                'command2': "ma_categorias",
                'ho_dato': "",
                'ca_dato': "",
                'se_dato': "",
                'no_dato': "",
                'pr_dato': "",
                'ay_dato': "",
                'bu_texto': "",
                'bu_marca': values[2],  # marca
                'bu_familia': values[0],  # categoria
                'bu_subfam': values[1],  # subcategoria
                'bu_stock': "",
                'bu_pagina': x,
                'bu_tipobus': "M",
                'bu_oferta': "",
                'cc_listado': "",
                'cc_opcion': "",
                'cc_nrocot': "",
                'cc_codart': "",
                'cc_cantidad': "",
                'cc_codsucursal': "",
                'cc_sucursal': "",
                'cc_oc': "",
                'cc_notaventa': "",
                'cc_observacion': "",
                'cc_coladespacho': "",
                'cc_indformadespacho': "",
                'cc_despachosparciales': "",
                'cc_usaflete': "",
                'cc_clientefinal': "",
                'cc_rutclientefinal': "",
                'cc_razonsocialclientefinal': "",
                'cc_direccionclientefinal': "",
                'cc_ciudadclientefinal': "",
                'cc_nombrecontactoclientefinal': "",
                'cc_telefonocontactoclientefinal': "",
                'cc_correocontactoclientefinal': "",
                'mo_tipo': "",
                'mo_nvp_NotaVenta': "",
                'mo_num_Factura': "",
                'mo_num_NotaCredito': "",
                'mo_erma_Rma': "",
                'of_dato': "",
                'textobusca': "",
                'marcabusca': "",  # marca
                'ca_familia': "",
                'ca_subfam': ""
            }
            c = session.post(URL, data=post_data)
            html2 = BeautifulSoup(c.text, "html.parser")
            productos_aux = sacarProductos(html2)

            for y in productos_aux:
                productos.append(y)

    else:
        productos = sacarProductos(html)
    return productos


def sacarProductos(html):
    productos = []
    trs_bg = html.find_all('tr', attrs={'bgcolor': '#f3f3f3'})
    trs_bg_other = html.find_all('tr', attrs={'bgcolor': '#ffffff'})

    for tr in trs_bg_other:
        trs_bg.append(tr)

    for tr in trs_bg:
        tds = BeautifulSoup(str(tr), "html.parser")
        tds = tds.find_all('td')
        n = 0
        producto = Producto()
        for td in tds:
            td = td.text
            td = td.replace("\t", "")
            td = td.strip().rstrip()
            if (n == 0):
                producto.codigotg = td
            if (n == 1):
                producto.partnumber = td
            if (n == 2):
                producto.descripcion = td
            if (n == 3):
                if "CL" in td:
                    producto.us = False
                elif "US" in td:
                    producto.us = True
                td = td.split(" ")
                producto.valor = td[1]
            if (n == 4):
                producto.stock = td
            n += 1
        print producto.partnumber, producto.stock, producto.us, producto.descripcion, producto.valor
        productos.append(producto)

    return productos


#########################

'''''''''
Arreglos de datos desde DB
'''''''''


def scrap():
    if (r.status_code == 200):
        # Extrayendo marcas
        html = BeautifulSoup(r.text, "html.parser")
        marcas = html.find_all('option')  # obtengo las marcas
        for marca in marcas:
            if marca['value'] != '0':
                tgMarcas.append(marca['value'].encode('UTF8'))
            if marca['value'] == '0':
                pass

        # print tgMarcas
        marcasVP = {}
        for tgmarca in tgMarcas:
            tgCat = {}
            # print "\t",tgmarca #marcas
            marca_data = {
                'command': "pa_marcas",
                'command2': "ma_home",
                'ho_dato': "",
                'ca_dato': "",
                'se_dato': "",
                'no_dato': "",
                'pr_dato': "",
                'ay_dato': "",
                'bu_texto': "",
                'bu_marca': tgmarca,  # marca
                'bu_familia': "",
                'bu_subfam': "",
                'bu_stock': "",
                'bu_pagina': "",
                'bu_tipobus': "M",
                'bu_oferta': "",
                'cc_listado': "",
                'cc_opcion': "",
                'cc_nrocot': "",
                'cc_codart': "",
                'cc_cantidad': "",
                'cc_codsucursal': "",
                'cc_sucursal': "",
                'cc_oc': "",
                'cc_notaventa': "",
                'cc_observacion': "",
                'cc_coladespacho': "",
                'cc_indformadespacho': "",
                'cc_despachosparciales': "",
                'cc_usaflete': "",
                'cc_clientefinal': "",
                'cc_rutclientefinal': "",
                'cc_razonsocialclientefinal': "",
                'cc_direccionclientefinal': "",
                'cc_ciudadclientefinal': "",
                'cc_nombrecontactoclientefinal': "",
                'cc_telefonocontactoclientefinal': "",
                'cc_correocontactoclientefinal': "",
                'mo_tipo': "",
                'mo_nvp_NotaVenta': "",
                'mo_num_Factura': "",
                'mo_num_NotaCredito': "",
                'mo_erma_Rma': "",
                'of_dato': "",
                'textobusca': "",
                'marcabusca': tgmarca,  # marca
                'ca_familia': "",
                'ca_subfam': ""
            }
            try:

                b = session.post(URL, data=marca_data)
                html2 = BeautifulSoup(b.text,
                                      "html.parser")  # No se repite mas abajo para no recargar la pagina en cada for

                divClTop = html2.find_all('div', class_='clTop')
            except Exception, e:
                print '[-] Error get data por marca: Marca ya no existe?; ', e
            # categorias
            catVP = {}
            for aux_cat in divClTop:

                cl_top = aux_cat

                clmain = BeautifulSoup(aux_cat.text, "html.parser")

                aux_cat = str(aux_cat)
                aux_cat = BeautifulSoup(aux_cat, "html.parser")
                aux_cat = aux_cat.find_all('a', class_='clMain')

                aux_cat = str(aux_cat)
                aux_cat = BeautifulSoup(aux_cat, "html.parser")
                aux_cat = aux_cat.find_all('img')

                aux_cat = str(aux_cat)
                cat = BeautifulSoup(aux_cat, "html.parser")

                cat = cat.text.replace("\\\\xa0", '').encode('UTF8')
                cat = cat.replace("[", '')
                cat = cat.replace("]", '')
                # print str(cat)  # Obtengo la categoria

                cl_subs = cl_top.find_all('div', class_='clSub')

                subcatVP = {}
                for aux_subcat in cl_subs:

                    aux_subcat = str(aux_subcat)
                    aux_subcat = BeautifulSoup(aux_subcat, "html.parser")
                    aux_subbcat = aux_subcat.find_all('a', class_='clSubb')
                    # print aux_subbcat
                    for subbcat in aux_subbcat:
                        # print subbcat.text #subbcat
                        text_subbcat = subbcat.text.encode('UTF8')
                        text_subbcat = text_subbcat.replace("\xa0", '')
                        text_subbcat = text_subbcat.replace("\xc2", '')
                        text_subbcat = elimina_tildes(text_subbcat.decode('utf8'))

                        lineahtml = str(subbcat)

                        regx = re.search(r"[0-9]{1,3}, [0-9]{1,3}, .[\w\s]{1,200}.", lineahtml)
                        if regx:
                            lineahtml = regx.group()
                            js_values = lineahtml.rstrip().strip()
                            js_values = js_values.replace('"', "")
                            js_values = js_values.split(",")
                            n = 0
                            for x in js_values:
                                js_values[n] = x.rstrip().strip()
                                n += 1

                            subcatVP[text_subbcat] = BuscarProductos(js_values)

                        else:
                            pass

                catVP[cat] = subcatVP
            marcasVP[tgmarca] = catVP
        return marcasVP


print time.strftime("%H:%M:%S")
cosas = scrap()  # Quizas escribir en un archivo?
print time.strftime("%H:%M:%S")

''''
INSERTANDO MARCA
'''''


def insertarMarca(marca):
    try:
        cur.execute("INSERT INTO oc_manufacturer (name, sort_order) values (%s, %s)", (marca, 0))
        cur.execute('select last_insert_id()')
        idmarca = cur.fetchall()
        idmarca = str(idmarca[0]).replace("(", '')
        idmarca = idmarca.replace(',', '')
        idmarca = idmarca.replace(')', '')
        cur.execute("INSERT INTO oc_manufacturer_to_store (manufacturer_id, store_id) values (%s, %s)",
                    ((idmarca), 0))  # store 0 es la por defecto
        return idmarca
    except Exception, e:
        print 'Error Insertar:', e
        sys.exit(1)


''''
INSERTAR CAT
'''


def insertarCat(cat):
    try:
        cur.execute(
            "insert into oc_category"
            " (image,parent_id,top,`column`,sort_order,status,date_added,date_modified) "
            "values "
            "(%s,%s,%s,%s,%s,%s,NOW(),NOW())",
            ('', 0, 0, 0, 0, 1))
        cur.execute('select last_insert_id()')
        idcat = cur.fetchall()
        idcat = str(idcat[0]).replace("(", '')
        idcat = idcat.replace(',', '')
        idcat = idcat.replace(')', '')
        name = cat
        descripcion = cat
        cur.execute(
            "insert into oc_category_description (category_id,language_id,name,description,meta_title,meta_description,meta_keyword) values (%s,%s,%s,%s,%s,%s,%s)",
            (idcat, 2, name, descripcion, name, name, name))
        cur.execute("insert into oc_category_to_store (category_id,store_id) values (%s,%s)", (idcat, 0))
        return idcat
    except Exception, e:
        print "Error insertar cat: ", e
        return False

'''''
INSERTANDO SUBCAT
'''''

def insertarSubCat(cat, parent):
    try:
        cur.execute(
            "insert into oc_category"
            " (image,parent_id,top,`column`,sort_order,status,date_added,date_modified) "
            "values "
            "(%s,%s,%s,%s,%s,%s,NOW(),NOW())",
            ('', parent, 0, 0, 0, 1))
        cur.execute('select last_insert_id()')
        idcat = cur.fetchall()
        idcat = str(idcat[0]).replace("(", '')
        idcat = idcat.replace(',', '')
        idcat = idcat.replace(')', '')
        name = cat
        descripcion = cat
        cur.execute(
            "insert into oc_category_description (category_id,language_id,name,description,meta_title,meta_description,meta_keyword) values (%s,%s,%s,%s,%s,%s,%s)",
            (idcat, 2, name, descripcion, name, name, name))
        cur.execute("insert into oc_category_to_store (category_id,store_id) values (%s,%s)", (idcat, 0))
        return idcat
    except Exception, e:
        print "Error insertar cat: ", e
        return False

'''''
INSERTAR PRDUCTO Y PROCESO DE NEGOCIO
'''''

def evaluaNeg(neto):
    neto = int(neto)
    utilidad = 0
    iva = 1.19
    arrValores = [
                [1, 30000, 1.3],
                [30001, 999999, 1.1],
                [1000000, 2000000, 1.23],
                [2000001, 200000000, 1.1]
                                          ]

    for data in arrValores:
        if (neto >= data[0] and neto <= data[1]):
            print 'utilidad de ', data[2]
            utilidad = data[2]
            break

    valorFinal = (neto * utilidad) * iva
    valorFinal = int(round(float(valorFinal),0))
    return valorFinal

def insertaProducto(prod, idmarca, idCat):
    prod.__class__ = Producto
    #Evaluando el valor devolviendo en pesos
    valor = prod.valor

    if prod.us:
        valor = valor.split(".")
        valor[0] = valor[0].replace(',', '')
        if int(valor[1])>0:
            valor = int(valor[0])+1
        else:
            valor = int(valor[0])
        valor = valor*tg_us #conversion a pesos
    else:
        valor = int(valor.replace(',', ''))

    valor = evaluaNeg(valor) #entrega valor final mas iva
    prod.valor = valor #guarda el valor en la clase

    try:
        cur.execute("SELECT product_id from oc_product WHERE partnumber='" + prod.partnumber + "'")
        rsProducto = cur.fetchall()
        okPass = 1
    except Exception, e:
        print "[-] Error en busqueda de producto: ", e
        okPass = 0
        rsProducto = False

    if rsProducto and okPass == 1:
        # SI LO ENCUENTRA LO UPDATEA
        idProducto = int(str(rsProducto[0]).replace('(', '').replace(')', '').replace(',', ''))  # Se updatea con este ID
        status = 0
        if int(prod.stock) >= 2 and int(prod.valor) > 0:
            status = 1
        try:
            cur.execute("UPDATE oc_product set "
                        " model = '%s', location='', quantity = %s, stock_status_id = '5', "
                        " manufacturer_id = %s , price = %s,"
                        " weight_class_id = '', length= '', width = '', height = '', length_class_id = '', status = %s,"
                        " date_added = NOW(),"
                        " date_modified = NOW(), date_available = NOW() "
                        " WHERE product_id = %s "
                        % (prod.descripcion, prod.stock, idmarca, prod.valor, status, idProducto))
            print "PASO UPDATE 1"
            cadenaDesc = prod.descripcion
            cadenaDesc = cadenaDesc.split()

            #Creamos las cadenas en base al split de desccripcion
            if len(cadenaDesc) >= 4:
                tags = cadenaDesc[0]+', '+cadenaDesc[1]+', '+cadenaDesc[2]+'-'+cadenaDesc[3]+', '+prod.partnumber
                metaTitle = cadenaDesc[0]+'-'+cadenaDesc[1]+'-'+cadenaDesc[2]+'-'+prod.partnumber
            elif len(cadenaDesc) == 3:
                tags = cadenaDesc[0]+', '+cadenaDesc[1]+', '+cadenaDesc[2]+', '+prod.partnumber
                metaTitle = cadenaDesc[0]+'-'+cadenaDesc[1]+'-'+prod.partnumber
            elif len(cadenaDesc) == 2:
                tags = cadenaDesc[0]+', '+cadenaDesc[1]+', '+prod.partnumber
                metaTitle = cadenaDesc[0]+'-'+cadenaDesc[1]+'-'+prod.partnumber
            else:
                tags = cadenaDesc[0]+','+prod.partnumber
                metaTitle = cadenaDesc[0]+'-'+prod.partnumber
            metaKeyword = prod.descripcion

            cur.execute("UPDATE oc_product_description set "
                        " name = '%s', meta_keyword = '%s', description = '%s', meta_title= '%s', tag = '%s' "
                        "WHERE product_id = %s " %
                        (prod.descripcion, metaKeyword, prod.descripcion, metaTitle, tags, idProducto))
            print "PASO UPDATE 2 id: ", idProducto
        except Exception, e:
            print '[-] Error: En Update de productos ->', e

    elif okPass == 1:
        ##SI NO LO ENCUENTRA LO INSERTARA
        status = 0
        if int(prod.stock) >= 2 and int(prod.valor) > 0:
            status = 1
        try:
            cur.execute("INSERT INTO oc_product "
                        "(`model`,`location`,`quantity`,`stock_status_id`,`image`,`manufacturer_id`,`price`,"
                        "`weight_class_id`,`length`,`width`,`height`,`length_class_id`,`status`,`date_added`,"
                        "`date_modified`,`viewed`, `date_available`, `partnumber`) "
                        "VALUES "
                        "(%s,'',%s,'5','',%s,%s,'1','0','0','0','0',%s,NOW(),NOW(),'0', NOW(), %s)",
                        (prod.descripcion, prod.stock, idmarca, prod.valor, status, prod.partnumber))
            print "PASO INSERT 1"
            cur.execute("select last_insert_id()")
            idProducto = cur.fetchall()
            idProducto = str(idProducto[0]).replace("(", '')
            idProducto = idProducto.replace(',', '')
            idProducto = idProducto.replace(')', '')
            print "INSERTANDO -> id insetado ", idProducto
            cadenaDesc = prod.descripcion
            cadenaDesc = cadenaDesc.split()

            #Creamos las cadenas en base al split
            if len(cadenaDesc) >= 4:
                seoUrl = cadenaDesc[0]+'-'+cadenaDesc[1]+'-'+cadenaDesc[2]+'-'+cadenaDesc[3]
                tags = cadenaDesc[0]+', '+cadenaDesc[1]+', '+cadenaDesc[2]+'-'+cadenaDesc[3]+', '+prod.partnumber
                metaTitle = cadenaDesc[0]+'-'+cadenaDesc[1]+'-'+cadenaDesc[2]+'-'+prod.partnumber
            elif len(cadenaDesc) == 3:
                seoUrl = cadenaDesc[0]+'-'+cadenaDesc[1]+'-'+cadenaDesc[2]
                tags = cadenaDesc[0]+', '+cadenaDesc[1]+', '+cadenaDesc[2]+', '+prod.partnumber
                metaTitle = cadenaDesc[0]+'-'+cadenaDesc[1]+'-'+prod.partnumber
            elif len(cadenaDesc) == 2:
                seoUrl = cadenaDesc[0]+'-'+cadenaDesc[1]
                tags = cadenaDesc[0]+', '+cadenaDesc[1]+', '+prod.partnumber
                metaTitle = cadenaDesc[0]+'-'+cadenaDesc[1]+'-'+prod.partnumber
            else:
                seoUrl = cadenaDesc[0]
                tags = cadenaDesc[0]+','+prod.partnumber
                metaTitle = cadenaDesc[0]+'-'+prod.partnumber
            metaKeyword = prod.descripcion

            cur.execute("INSERT INTO `oc_product_to_category` (`product_id`, `category_id`)"
                        " VALUES (%s, %s)", (idProducto, idCat))
            print "PASO INSERT 2"
            cur.execute("INSERT INTO `oc_product_to_store` "
                        "(`product_id`, `store_id`) VALUES (%s,%s)", (idProducto, '0'))
            print "PASO INSERT 3"
            cur.execute("INSERT INTO `oc_url_alias` (`query`, `keyword`) VALUES (%s,%s)", ('product_id='+idProducto, seoUrl))
            print "PASO INSERT 4"
            cur.execute("INSERT INTO `oc_product_description`"
                        " (`product_id`,`language_id`,`name`,`meta_keyword`,`description`, `meta_title`, `tag`) "
                        "VALUES (%s, %s, %s, %s, %s, %s, %s)", (idProducto, '2', prod.descripcion, metaKeyword, prod.descripcion, metaTitle, tags))
            print "PASO INSERT 5"
            print 'Prduct id ->', idProducto
        except Exception, e:
            print '[-] Error: En insert de productos ->', e






''''
ACA TRAER LAS MARCAS Y CATEGORIAS DESDE EL CMS, DENTRO DE CADA FOR PREGUNTAR SI EXISTE ESTA MARCA O CATEGORIA SI NO EXISTE SE DEBE CREAR,

PARA EL PRODUCTO SI EXISTE SE UPDATEA Y SI NO, SE INSERTA
'''''
cur.execute('SELECT name, manufacturer_id FROM oc_manufacturer')
dbmarcas = cur.fetchall()

cur.execute(
    'select name, a.category_id from oc_category_description b, oc_category a where b.category_id = a.category_id')
dbcategorias = cur.fetchall()

for marca in cosas:
    print '-', marca
    idmarca = 0
    okz = 0
    for row in dbmarcas:
        if marca in row[0]:
            okz = 1
            idmarca = row[1]
            break

    if okz == 1:
        pass
    else:
        idmarca = insertarMarca(marca)

    for cate in cosas[marca]:
        idcat = 0
        ok = 0
        for row in dbcategorias:
            if cate in row[0]:
                ok = 1
                idcat = row[1]
                break

        if ok == 1:
            pass
        else:
            idcat = insertarCat(cate)
            cur.execute(
                'select name, a.category_id from oc_category_description b, oc_category a where b.category_id = a.category_id')
            dbcategorias = cur.fetchall()  # recargo el arreglo de cats
        print '--', cate, '-->', idcat
        for subcategoria in cosas[marca][cate]:
            idsubcat = 0
            oks = 0
            for row in dbcategorias:
                if subcategoria in row[0]:
                    oks = 1
                    idsubcat = row[1]
                    break

            if oks == 1:
                pass
            else:
                idsubcat = insertarSubCat(subcategoria, idcat)
                cur.execute(
                    'select name, a.category_id from oc_category_description b, oc_category a where b.category_id = a.category_id')
                dbcategorias = cur.fetchall()  # recargo el arreglo de cats
            print '---', subcategoria, ' id ->', idsubcat, ' parent ->', idcat
            for producto in cosas[marca][cate][subcategoria]:
                producto.__class__ = Producto
                try:
                    insertaProducto(producto, idmarca, idsubcat)
                except Exception, e:
                    print '[-]Error insproducto(): ', e

print '[+]TERMINADO A LAS: ', time.strftime("%H:%M:%S")

'''' EXTRAER LOS PRODUCTOS
for x in subcatVP[text_subbcat]:
    x.__class__ = Producto
    print x.us, x.descripcion, x.valor
'''''

